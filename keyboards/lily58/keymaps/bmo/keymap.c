#include QMK_KEYBOARD_H

#ifdef OLED_ENABLE
#include "animation/luna.c"
#endif // OLED_ENABLE

#ifdef OS_DETECTION_ENABLE
#include "os_detection.h"
#endif // OS_DETECTION_ENABLE

#include "process_keycode/process_auto_shift.h"

enum custom_keycodes {
  KC_GAME = SAFE_RANGE
};

enum layer_number {
  _GAME = 0,
  _QWERTY,
  _LOWER,
  _RAISE,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* GAME
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * | ESC  |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |  `   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  |   Q  |   W  |   E  |   R  |   T  |                    |   Y  |   U  |   I  |   O  |   P  |  -   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |LCTRL |   A  |   S  |   D  |   F  |   G  |-------.    ,-------|   H  |   J  |   K  |   L  |   ;  |  '   |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |LShift|   Z  |   X  |   C  |   V  |   B  |-------|    |-------|   N  |   M  |   ,  |   .  |   /  |RShift|
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | / Space /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
  [_GAME] = LAYOUT(
  KC_ESC,   KC_1,   KC_2,    KC_3,    KC_4,    KC_5,                     KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_GRV,
  KC_TAB,   KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,                     KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_MINS,
  KC_LCTL,  KC_A,   KC_S,    KC_D,    KC_F,    KC_G,                     KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,
  KC_LSFT,  KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, KC_LBRC,  KC_RBRC,  KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH,  KC_RSFT,
                    KC_LALT, KC_LGUI, MO(_LOWER), KC_SPC, KC_ENT, MO(_RAISE), KC_BSPC, KC_RGUI
  ),
/* QWRTY
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------.    ,-------|      |      |      |      |      |      |
 * |------+------+------+------+------+------|       |    |       |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|      |      |      |      |      |      |
 * `-----------------------------------------/  MT   /     \      \-----------------------------------------'
 *                   |      |      |      | /  LCTL /       \      \  |      |      |      |
 *                   |      |      |      |/   SPC /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
 [_QWERTY] = LAYOUT(
  _______, _______, _______, _______, _______, _______,                                _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______, _______, _______,                                _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______, _______, _______,                                _______, _______, _______, _______, _______, _______,
  _______, _______, _______, _______, _______, _______, _______,              _______, _______, _______, _______, _______, _______, _______,
                             _______, _______, _______, MT(MOD_LCTL, KC_SPC), _______, _______, _______, _______
),
/* LOWER
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |                    |  F7  |  F8  |  F9  | F10  | F11  | F12  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   `  |   !  |   @  |   #  |   $  |   %  |-------.    ,-------|   ^  |   &  |   *  |   (  |   )  |      |
 * |------+------+------+------+------+------|   (   |    |    )  |------+------+------+------+------+------|
 * |      |      | Cut  | Copy |Paste |      |-------|    |-------|      |      |      |      |      |      |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | / Space /       \Enter \  |RAISE |  Del | RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
[_LOWER] = LAYOUT(
  TG(_QWERTY), _______, _______, _______, _______,  _______,                   _______, _______, _______,_______, _______,  _______,
  KC_F1,       KC_F2,   KC_F3,   KC_F4,   KC_F5,    KC_F6,                     KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,
  KC_GRV,      KC_EXLM, KC_AT,   KC_HASH, KC_DLR,   KC_PERC,                   KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, XXXXXXX,
  _______,     _______, KC_CUT,  KC_COPY, KC_PASTE, _______, KC_LPRN, KC_RPRN, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
                                 _______, _______,  _______, _______, _______, _______, KC_DEL, _______
),
/* RAISE
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   `  |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |  F12 |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |-------.    ,-------| Left | Down |  Up  |Right |   ~  |   |  |
 * |------+------+------+------+------+------|   (   |    |    )  |------+------+------+------+------+------|
 * |  F7  |  F8  |  F9  | F10  | F11  | F12  |-------|    |-------|   +  |   -  |   =  |      |   \  |      |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /  Tab  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
[_RAISE] = LAYOUT(
  TG(_QWERTY), _______, _______, _______, _______, _______,                     _______, _______, _______, _______, _______, _______,
  KC_GRV,      KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                        KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_F12,
  KC_F1,       KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,                       KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, KC_TILD, KC_PIPE,
  KC_F7,       KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_LPRN,  KC_RPRN,  KC_PLUS, KC_MINS, KC_EQL,  XXXXXXX, KC_BSLS, XXXXXXX,
                                 _______, _______, _______, KC_TAB,   _______,  _______, _______, _______
)
};

layer_state_t layer_state_set_user(layer_state_t state) {
  switch (get_highest_layer(state)) {
    case _QWERTY:
	  autoshift_enable();
      break;
    default:
	  autoshift_disable();
  }
  return state;
};


//layer_state_t layer_state_set_user(layer_state_t state) {
//  return update_tri_layer_state(state, _LOWER, _RAISE, _GAME);
//}

//SSD1306 OLED update loop, make sure to enable OLED_ENABLE=yes in rules.mk
#ifdef OLED_ENABLE

//oled_rotation_t oled_init_user(oled_rotation_t rotation) {
//  if (!is_keyboard_master())
//    return OLED_ROTATION_180;  // flips the display 180 degrees if offhand
//  return rotation;
//}

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
    return OLED_ROTATION_270;
}

bool oled_task_user(void) {

  // Display dected OS
  switch (detected_host_os()) {
    case OS_MACOS:
    case OS_IOS:
      //oled_write_raw_P(mac_logo, sizeof(mac_logo));
      oled_write_P(PSTR(" MAC\n"), false);
      break;
    case OS_LINUX:
      oled_write_P(PSTR("LINUX\n"), false);
      break;
    default:
      break;
  }

  // Display Active layer
  switch (get_highest_layer(layer_state)) {
    case _QWERTY:
        oled_write_ln_P(PSTR("QWRTY"), false);
        break;
    case _LOWER:
        oled_write_P(PSTR("LOWER"), false);
        break;
    case _RAISE:
        oled_write_ln_P(PSTR("RAISE"), false);
        break;
    case _GAME:
        oled_write_ln_P(PSTR("GAME"), false);
        break;
    default:
      // Or use the write_ln shortcut over adding '\n' to the end of your string
      oled_write_ln_P(PSTR("UNDEF"), false);
  }

  /* KEYBOARD PET VARIABLES START */
  current_wpm   = get_current_wpm();
  led_usb_state = host_keyboard_led_state();
  render_animation(0, 13);
  /* KEYBOARD PET RENDER END */

  return false;
}
#endif // OLED_ENABLE

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case KC_COPY:
      if (record->event.pressed) {
        switch (detected_host_os()) {
          case OS_MACOS:
          case OS_IOS:
            SEND_STRING(SS_LGUI("c"));
            break;
          default:
            break;
        }
      }
      break;
    case KC_CUT:
      if (record->event.pressed) {
        switch (detected_host_os()) {
          case OS_MACOS:
          case OS_IOS:
            SEND_STRING(SS_LGUI("x"));
            break;
          default:
            break;
        }
      }
      break;
    case KC_PASTE:
      if (record->event.pressed) {
        switch (detected_host_os()) {
          case OS_MACOS:
          case OS_IOS:
            SEND_STRING(SS_LGUI("v"));
            break;
          default:
            break;
        }
      }
      break;

    /* KEYBOARD PET STATUS START */
    case KC_LCTL:
    case KC_RCTL:
      if (record->event.pressed) {
          isSneaking = true;
      } else {
          isSneaking = false;
      }
      break;
    case KC_SPC:
      if (record->event.pressed) {
          isJumping  = true;
          showedJump = false;
      } else {
          isJumping = false;
      }
      break;
    /* KEYBOARD PET STATUS END */
    case KC_GAME:
      autoshift_toggle();
      break;
    default:
      break;
  }
  return true;
}
